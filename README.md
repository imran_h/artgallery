# ArtGallery

### Overview

*This is a web based project

*This is ASP.NET

*Thus is built to manage a Art Gallery
*Here Admin can post images of various types of art
*Users can rate and buy the arts

###Prerequsite
*Microsoft Visual Studio
*MySql server

###Setup
*Run artgallery.sql in MySql server
*Open ArtGalleryV2.sln in Visual studio

###Team Member
####Imran Hossain
####Al-Amin
####Tanmain Kumar Ghosh

###Project Supervisor
####Prof. Dr. Anisur Rahman
  Professor<br/>
  Computer Science and Engineering Discipline<br/>
  Khulna University, Bangladesh

Thank You
  
